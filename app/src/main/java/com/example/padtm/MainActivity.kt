package com.example.padtm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private fun showToast(msg: String): Unit {
        val toast = Toast.makeText(this, msg, Toast.LENGTH_LONG)
        toast.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("LAB2", "onCreate()")
        showToast("onCreate()")
    }

    override fun onResume() {
        super.onResume()
        Log.d("LAB2", "onResume()")
        showToast("onResume()")
    }

    override fun onStop() {
        super.onStop()
        Log.d("LAB2", "onStop() - Activity no longer visible")
        showToast("onStop()")
    }

    override fun onPause() {
        super.onPause()
        Log.d("LAB2", "onPause() - Activity in the background")
        showToast("onPause()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("LAB2", "onDestroy() - Activity destroyed")
        showToast("onDestroy()")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("LAB2", "onRestart() - Activity visible again")
        showToast("onRestart()")
    }
}